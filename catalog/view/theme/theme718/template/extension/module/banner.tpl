<div class="banners-wrap">
<?php foreach ($banners as $banner) { ?>
<?php if ($banner['link']) { ?>
<div class="banners <?php echo $banner['title']; ?>">
	<a class="clearfix" href="<?php echo $banner['link']; ?>">
		<img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
		<?php if ($banner['description']) { ?>
		<div class="s-desc"><?php echo $banner['description']; ?></div>
		<?php } ?>
	</a>
</div>
<?php } else { ?>
<div class="banners <?php echo $banner['title']; ?>">
	<img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
	<?php if ($banner['description']) { ?>
	<div class="s-desc"><?php echo $banner['description']; ?></div>
	<?php } ?>
</div>
<?php } ?>
<?php } ?>
</div>