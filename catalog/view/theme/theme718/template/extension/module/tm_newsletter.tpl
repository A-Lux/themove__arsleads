<div id="tm-newsletter" class="box newsletter">
	<div class="box-content">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-5 text-right">
					<?php if ($description){?><p class="newsletter-description"><?php echo $description;?></p><?php }?>
				</div>
				<form method="post" enctype="multipart/form-data" id="tm-newsletter-form">
					<div class="col-xs-7 col-sm-8 col-md-4">
						<div class="tm-login-form">
							<!-- <label class="control-label" for="input-tm-newsletter-email"></label> -->
							<input type="text" name="tm_newsletter_email" value="" placeholder="<?php echo $entry_mail; ?>"
							id="input-tm-newsletter-email" class="form-control"/>
						</div>
						<span id="tm-newsletter_error" class="newsletter-error"></span>
						<span id="tm-newsletter_success" class="newsletter-success"></span>
					</div>
					<div class="col-xs-5 col-sm-4 col-md-3">
						<button type="submit" id="tm-newsletter-button" class="dropdown-btn">
						<i class="material-design-drafts"></i><?php echo $button_subscribe; ?></button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

	<script>
		$(document).ready(function () {
			$('#tm-newsletter').submit(function (e) {
				e.preventDefault();
				var email = $("#input-tm-newsletter-email").val();
				var emailRegex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}$/igm;
				if (emailRegex.test(email)) {
					var dataString = 'tm_newsletter_email=' + email;
					$.ajax({
						type: "POST",
						url: "index.php?route=extension/module/tm_newsletter/addNewsletter",
						data: dataString,
						cache: false,
						success: function (result) {
							if (!result){
								$('#tm-newsletter_error').html('');
								$('#tm-newsletter_success').stop(true, true).html('<?php echo $success; ?>').fadeIn(300).delay(4000).fadeOut(300);
							}else{
								$('#tm-newsletter_success').html('');
								$('#tm-newsletter_error').stop(true, true).html(result).fadeIn(300).delay(4000).fadeOut(300);
							}
						}
					});
				} else {
					$('#tm-newsletter_success').html('');
					$('#tm-newsletter_error').stop(true, true).html('<?php echo $error_invalid_email; ?>').fadeIn(300).delay(4000).fadeOut(300);
				}
			});
		});
	</script>