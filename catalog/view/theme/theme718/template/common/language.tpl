<?php if (count($languages) > 1) { ?>
<div class="box-language">
	<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-language">
		<div class="btn-group toggle-wrap hidden-xs hidden-sm">
			<span class="dropdown-toggle" data-toggle="dropdown">
				<?php foreach ($languages as $language) { ?>
				<?php if ($language['code'] == $code) { ?>
				<span><?php echo $text_language; ?></span>
				<?php } ?>
				<?php } ?>
				<span class="hidden-xs hidden-sm hidden-md hidden"><?php echo $text_language; ?></span>
			</span>
			<ul class="dropdown-menu list-unstyled">
				<?php foreach ($languages as $language) { ?>
				<?php if ($language['code'] == $code) { ?>
				<li>
					<button class="language-select selected" type="button" name="<?php echo $language['code']; ?>">
						<?php echo $language['name']; ?>
					</button>
				</li>
				<?php } else { ?>
				<li>
					<button class="language-select" type="button" name="<?php echo $language['code']; ?>">
						<?php echo $language['name']; ?>
					</button>
				</li>
				<?php } ?>
				<?php } ?>
			</ul>
		</div>
		<ul class="hidden-md hidden-lg">
			<?php foreach ($languages as $language) { ?>
			<?php if ($language['code'] == $code) { ?>
			<li>
				<button class="language-select selected" type="button" name="<?php echo $language['code']; ?>">
					<?php echo $language['name']; ?>
				</button>
			</li>
			<?php } else { ?>
			<li>
				<button class="language-select" type="button" name="<?php echo $language['code']; ?>">
					<?php echo $language['name']; ?>
				</button>
			</li>
			<?php } ?>
			<?php } ?>
		</ul>
		<input type="hidden" name="code" value="" />
		<input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
	</form>
</div>
<?php } ?>