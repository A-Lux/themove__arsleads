<?php echo $header; ?>
<?php if ($top) { ?>
    <section class="top">
        <?php echo $top; ?>
    </section>
<?php } ?>
<section id="container">
    <div class="container">
        <div class="row"><?php echo $column_left; ?>
            <?php if ($column_left && $column_right) { ?>
            <?php $class = 'col-sm-6'; ?>
            <?php } elseif ($column_left || $column_right) { ?>
            <?php $class = 'col-sm-9'; ?>
            <?php } else { ?>
            <?php $class = 'col-sm-12'; ?>
            <?php } ?>
            <div id="content" class="<?php echo $class; ?>"><div class="clear"></div><?php echo $content_top; ?></div>
            <?php echo $column_right; ?>
        </div>
    </div>
</section>
<section class="content_bottom">
    <?php echo $content_bottom; ?>
</section>
<?php if ($stait_bottom) { ?>
    <section class="container_bottom">
        <?php echo $stait_bottom; ?>
    </section>
<?php } ?>
<?php echo $footer; ?>
<?php if ($bottom) { ?>
    <section class="bottom">
        <?php echo $bottom; ?>
    </section>
<?php } ?>