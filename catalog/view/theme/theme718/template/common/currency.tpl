<?php if (count($currencies) > 1) { ?>
	<li class="box-currency">
		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-currency">
			<div class="btn-group hidden-xs hidden-sm">
				<span class="dropdown-toggle">
					<?php foreach ($currencies as $currency) { ?>
					<?php if ($currency['symbol_left'] && $currency['title'] == $code) { ?>
					<?php echo $currency['symbol_left']; ?>
					<?php } elseif ($currency['symbol_right'] && $currency['title'] == $code) { ?>
					<?php echo $currency['symbol_right']; ?>
					<?php } ?>
					<?php } ?>
					<span class=""><?php echo $text_currency; ?></span>
				</span>
				<ul class="dropdown-menu list-unstyled">
					<?php foreach ($currencies as $currency) { ?>
						<?php if ($currency['symbol_left']) { ?>
							<?php if ($currency['code'] == $code) { ?>
							<li>
								<button class="currency-select select" type="button" name="<?php echo $currency['code']; ?>">
									<?php echo $currency['symbol_left']; ?>
									<?php echo $currency['title']; ?>
								</button>
							</li>
							<?php } else { ?>
							<li>
								<button class="currency-select" type="button" name="<?php echo $currency['code']; ?>">
									<?php echo $currency['symbol_left']; ?>
									<?php echo $currency['title']; ?>
								</button>
							</li>
							<?php } ?>
						<?php } else { ?>
							<?php if ($currency['code'] == $code) { ?>
							<li>
								<button class="currency-select select" type="button" name="<?php echo $currency['code']; ?>">
									<?php echo $currency['symbol_right']; ?>
									<?php echo $currency['title']; ?>
								</button>
							</li>
							<?php } else { ?>
							<li>
								<button class="currency-select" type="button" name="<?php echo $currency['code']; ?>">
									<?php echo $currency['symbol_right']; ?>
									<?php echo $currency['title']; ?>
								</button>
							</li>
							<?php } ?>
						<?php } ?>
					<?php } ?>
				</ul>
			</div>
			<ul class="list-unstyled hidden-md hidden-lg">
					<?php foreach ($currencies as $currency) { ?>
						<?php if ($currency['symbol_left']) { ?>
							<?php if ($currency['code'] == $code) { ?>
							<li>
								<button class="currency-select select" type="button" name="<?php echo $currency['code']; ?>">
									<?php echo $currency['symbol_left']; ?>
									<?php echo $currency['title']; ?>
								</button>
							</li>
							<?php } else { ?>
							<li>
								<button class="currency-select" type="button" name="<?php echo $currency['code']; ?>">
									<?php echo $currency['symbol_left']; ?>
									<?php echo $currency['title']; ?>
								</button>
							</li>
							<?php } ?>
						<?php } else { ?>
							<?php if ($currency['code'] == $code) { ?>
							<li>
								<button class="currency-select select" type="button" name="<?php echo $currency['code']; ?>">
									<?php echo $currency['symbol_right']; ?>
									<?php echo $currency['title']; ?>
								</button>
							</li>
							<?php } else { ?>
							<li>
								<button class="currency-select" type="button" name="<?php echo $currency['code']; ?>">
									<?php echo $currency['symbol_right']; ?>
									<?php echo $currency['title']; ?>
								</button>
							</li>
							<?php } ?>
						<?php } ?>
					<?php } ?>
				</ul>
			<input type="hidden" name="code" value="" />
			<input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
		</form>
	</li>
<?php } ?>
