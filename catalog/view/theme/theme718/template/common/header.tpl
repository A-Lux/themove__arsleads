<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<?php $page_direction == 'rtl' ? $direction = 'rtl' : ''; ?>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="<?php echo $responsive ? 'mobile-responsive-off' : ''; ?>">
<!--<![endif]-->
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo $title; ?></title>
	<base href="<?php echo $base; ?>" />
	<?php if ($description) { ?>
	<meta name="description" content="<?php echo $description; ?>" />
	<?php } ?>
	<?php if ($keywords) { ?>
	<meta name="keywords" content= "<?php echo $keywords; ?>" />
	<?php } ?>
	<?php foreach ($add_this_meta as $meta) { ?>
	<meta property="og:<?php echo $meta['property'] ?>" content="<?php echo $meta['content'] ?>" /> 
	<?php } ?>
		<!-- Fonts -->
		<link href="//fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
		<link href="//fonts.googleapis.com/css?family=PT+Serif:400,700&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet"> 
		<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<!-- END Fonts -->
	<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />

	<?php if ($direction == 'rtl') { ?>
	<link href="catalog/view/theme/<?php echo $theme_path; ?>/stylesheet/bootstrap-rtl.css" rel="stylesheet">
	<?php } ?>

	<link href="catalog/view/theme/<?php echo $theme_path; ?>/js/owl.carousel/assets/owl.carousel.css" rel="stylesheet">
	<link href="catalog/view/theme/<?php echo $theme_path; ?>/stylesheet/material-design.css" rel="stylesheet">
	<link href="catalog/view/theme/<?php echo $theme_path; ?>/js/fancybox/jquery.fancybox.css" rel="stylesheet">
	<link href="catalog/view/theme/<?php echo $theme_path; ?>/stylesheet/photoswipe.css" rel="stylesheet">
	<link href="catalog/view/theme/<?php echo $theme_path; ?>/stylesheet/stylesheet.css" rel="stylesheet">
	<link href="catalog/view/theme/<?php echo $theme_path; ?>/stylesheet/fl-36-slim-icons.css" rel="stylesheet">
	<link href="catalog/view/theme/<?php echo $theme_path; ?>/stylesheet/fl-bigmug-line.css" rel="stylesheet">	

	<?php if ($direction == 'rtl') { ?>
	<link href="catalog/view/theme/<?php echo $theme_path; ?>/stylesheet/rtl.css" rel="stylesheet">
	<?php } ?>

	<?php foreach ($styles as $style) { ?>
	<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
	<?php } ?>
	<?php foreach ($links as $link) { ?>
	<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
	<?php } ?>

	<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
	<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

	<?php foreach ($scripts as $script) { ?>
	<script src="<?php echo $script; ?>" type="text/javascript"></script>
	<?php } ?>
	<?php foreach ($analytics as $analytic) { ?>
	<?php echo $analytic; ?>
	<?php } ?>
</head>
<body class="<?php echo $class; ?>">
	<p id="gl_path" class="hidden"><?php echo $theme_path; ?></p>
	<div id="page">
		<div id="page-preloader" class="visible"><span class="preloader"></span></div>
		<header id="header">
			<?php if ($header_top){?>
			<div id="header_modules" class="header_modules"><?php echo $header_top;?></div>
			<?php }?>
			<div id="stuck" class="stuck-menu">
				<div class="bg-dark">
					<div class="menu-toggle-wrap">
						<div class="menu-toggle">
							<i class="fa fa-bars"></i>
						</div>
						<div class="toggle-cont-menu">
							<?php if ($navigation){?>
								<div class="navigation">
									<div class="container">
										<?php echo $navigation;?>
									</div>
								</div>
							<?php }?>
						</div>
					</div>					
				<div class="container">
					<div class="row">				
					    <div class="col-xs-6 col-sm-3">
							<div id="logo" class="logo">
								<?php if ($logo) { ?>
								<a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive"></a>
								<?php } else { ?>
								<a href="<?php echo $home; ?>"><?php echo $name; ?></a>
								<?php } ?>
							</div>
					    </div>
					    <div class="col-xs-6 col-sm-5">
							<?php echo $search; ?>
						</div>
						<div class="col-xs-4">
							<nav id="top-links" class="nav toggle-wrap">
								<a class="toggle fa fa-gear" href='#'></a>
								<div class="toggle_cont">
									<ul class="list-unstyled">
										<?php if ($logged) { ?>
										<li> <a href="<?php echo $account; ?>"><?php echo $text_account; ?></a> </li>
										<li> <a href="<?php echo $order; ?>"><?php echo $text_order; ?></a> </li>
										<li> <a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a> </li>
										<li> <a href="<?php echo $download; ?>"><?php echo $text_download; ?></a> </li>
										<li> <a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a> </li>
										<?php } else { ?>										
										<li> <a href="<?php echo $login; ?>"><?php echo $text_login; ?></a> </li>
										<li> <a href="<?php echo $register; ?>"><?php echo $text_register; ?></a> </li>
										<?php } ?>
										<li> <a href="<?php echo $wishlist; ?>"> <?php echo $text_wishlist; ?> </a> </li>
										<li> <a href="<?php echo $checkout; ?>" > <?php echo $text_checkout; ?> </a> </li>
									</ul>
									<ul class="list-unstyled">											
										<?php echo $language; ?>
										<?php echo $currency; ?>
									</ul>										
								</div>								
							</nav>	
							<?php echo $cart; ?>					
						</div>
					</div>
				</div>
			</div>
			</div>
		</header>
		<script>
			;(function ($) {
				if ($('.common-home').length) {
					function headerWidth() {
						$('#header').css('min-height', $('#header_modules').height());
					}
					$(window).on('load resize', headerWidth);
				}
			})(jQuery);
		</script>